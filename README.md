# Template TP 2 DDP 2

## Menggunakan repo

- Jalankan `git remote add upstream https://gitlab.com/DDP2-CSUI/ddp-2-2022-2023-ganjil/template-tp2-ddp-2.git` lalu jalankan `git pull upstream master` untuk memasukkan kode dari repo ini ke repo Anda
- Commit dan push pekerjaan anda ke repo anda masing-masing

## Menjalankan program

- Anda dapat menjalankan program dengan menjalankan perintah `.\\gradlew.bat run` untuk Windows atau `./gradlew run` untuk UNIX-based OS

## Menjalankan unit test

- Anda dapat menjalankan program dengan menjalankan perintah `.\\gradlew.bat run` untuk Windows atau `./gradlew run` untuk UNIX-based OS
